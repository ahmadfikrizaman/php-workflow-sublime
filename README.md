# Better PHP Workflow (As suggested by Jeffrey Way)

### Linux on Ubuntu

```sh
$ cd ~/.config/sublime-text-3/Packages/User/
$ git clone https://bitbucket.org/ahmadfikrizaman/php-workflow-sublime.git
```

## Mac OSX (Todos)

```sh
```

### Install [PackageControl]

Open sublime and press **ctrl+`** to open console. Paste the content and let package controll install.

Open [PackageControl], install.


### Install Package

- HTML Prettify
- phpfmt
- Blade Spacer
- Bootstrap 3 snippets
- Laravel 5 artisan
- Laravel 5 snippets
- Laravel Blade Highlighter
- PHP Companion
- PHP Getters and Setters
- PHP Syntax Checker
- DocBlockr
- Livereload


### Install Theme
- Afterglow

### HTML Prettify Setting

- Set NodeJS path


### PHPFMT Setting
- Ctrl + Shit + P
- Toggle PSR-2

```json
{
    "autocomplete": true,
    "autoimport": true,
    "enable_auto_align": true,
    "format_on_save": true,
    "psr2": true,
    "smart_linebreak_after_curly": true,
    "version": 1,
}
```


### Key Bindings

```json
[
    { "keys": ["f9"], "command": "expand_fqcn" },
    { "keys": ["f10"], "command": "find_use" },
    { "keys": ["f7"], "command": "insert_php_constructor_property" },
]
```

### User Settings

```json
{
    "always_show_minimap_viewport": true,
    "color_inactive_tabs": true,
    "color_scheme": "Packages/Color Scheme - Default/Monokai.tmTheme",
    "ignored_packages":
    [
        "PHP",
        "Vintage"
    ],
    "line_padding_bottom": 3,
    "line_padding_top": 3,
    "overlay_scroll_bars": "enabled",
    "sidebar_row_padding_medium": true,
    "sidebar_size_12": true,
    "status_bar_brighter": true,
    "tabs_medium": true,
    "tabs_padding_medium": true,
    "theme": "Afterglow.sublime-theme",
    "translate_tabs_to_spaces": true,
}
```

### By

[whop.io]

### License

MIT

[whop.io]: <https://whop.io>
[PackageControl]: <https://packagecontrol.io/>